<?php

namespace extensions\keyCloak;


use Guzzle\Http\Client;
use Guzzle\Http\Exception\BadResponseException;
use Psr\Log\LoggerInterface;

class KeyCloak
{
    /**
     * @var Client Http-клиент для взаимодействия с api
     */
    protected $httpClient;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct($url, $logger)
    {
        $this->httpClient = new Client($url, [
            'ssl.certificate_authority' => false
        ]);
        $this->httpClient->setDefaultOption('headers', [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json'
        ]);
        $this->logger = $logger;
    }

    protected function sendRequest($url, $data = [])
    {
        try {
            $response = $this->httpClient->post($url, [], $data)->send();
            $this->logger->info("URL: $url status: {$response->getStatusCode()} response: {$response->getBody()}");
        } catch (BadResponseException $e) {
            $this->logger->error($e->getMessage());
        }
        return !empty($response) ? $response->json() : [];
    }

    public function getToken($clientId, $clientSecret)
    {
        return $this->sendRequest('', [
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'grant_type' => 'client_credentials'
        ]);
    }

    public function refreshToken($clientId, $clientSecret, $refreshToken)
    {
        return $this->sendRequest('', [
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'grant_type' => 'refresh',
            'refresh_token' => $refreshToken
        ]);
    }
}