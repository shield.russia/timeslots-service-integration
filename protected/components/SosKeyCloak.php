<?php

namespace components;

use CApplicationComponent;
use extensions\keyCloak\KeyCloak;
use Yii;

/**
 * класс авторизации в системе слотирования SmartOrderSystem
 * Class SosKeyCloak
 * @package components
 */
class SosKeyCloak extends CApplicationComponent
{
    public $clientId;
    public $clientSecret;
    public $url;

    /**
     * @var KeyCloak
     */
    protected $keyCloak;

    public function init()
    {
        parent::init();
        $logger = new DefaultLogger('SosKeyCloak');
        $logger->info('SosKeyCloak');
        $this->keyCloak = new KeyCloak($this->url, $logger);
    }

    public function getToken()
    {
        $key = __METHOD__;
        $token = Yii::app()->cache->get($key);

        if (!$token) {
            $token = $this->keyCloak->getToken($this->clientId, $this->clientSecret);
            Yii::app()->cache->set($key, $token, 600);
        }

        return $token;
    }

    public function getAccessToken()
    {
        $token = $this->getToken();

        return $token['access_token'] ?? null;
    }

}