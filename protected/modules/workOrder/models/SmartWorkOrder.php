<?php

namespace crm\modules\workOrder\models;

use ActiveResource;
use CMap;
use EActiveResourceRequest;
use EActiveResourceRequestException;
use DateTime;
use Yii;

/**
 * http://smart-wo.rmsi.ntk.novotelecom.ru:8081/swagger-ui.html#/
 * Часы, доступные для назначения времени визита
 */
class SmartWorkOrder extends ActiveResource
{
    public $id;

    /**
     * @param string $className
     * @return ActiveResource
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function connectionName()
    {
        return 'SmartWO';
    }

    public function rest()
    {
        return CMap::mergeArray(parent::rest(), [
            'idProperty' => 'id',
            'multiContainer' => 'response',
            'resource' => 'orders',
        ]);
    }

    public function patchRequest($route, $params = array(), $data = null)
    {
        $uri = $this->buildUri($route);
        return $this->sendRequest($uri, 'PATCH', $params, $data);
    }

    public function sendRequest($uri, $method, $params, $data)
    {
        if (!empty($params))
            $uri = $uri . '?' . http_build_query($params);

        $request = new EActiveResourceRequest;
        $request->setUri($uri);
        $request->setHeaderField('Authorization', 'Bearer ' . Yii::app()->sosKeyCloak->getAccessToken());
        $request->setMethod($method);
        $request->setData($data);
        return $this->getConnection()->execute($request);
    }


    public function getOrder($id)
    {
        try {
            $result = $this->getRequest($this->getSite() . '/orders/' . $id);
        } catch (EActiveResourceRequestException $e) {
            throw new EActiveResourceRequestException($e->getMessage(), $e->getCode());
        }

        return $result->getData();
    }

    /**
     * Сортировка и группировка интервалов
     * @param $order
     * @return array
     */
    public function prepareAvailableHours($order)
    {
        $intervals = $order['availableIntervals'];

        $duration = $order['duration'] * 60;
        $dates = [];

        foreach ($intervals as $interval) {
            $startDate = strtotime($interval['start']);
            $date = date('Y-m-d', $startDate);
            if (!in_array($date, $dates)) {
                array_push($dates, $date);
            }
        }

        sort($dates);

        $result = [];

        for ($i = 0; $i < count($dates); $i++) {

            $date = $dates[$i];

            $hours = [];
            $excluded = [];

            $selectedIntervals = array_filter($intervals, function ($interval) use ($date) {
                $startDate = date('Y-m-d', strtotime($interval['start']));
                return $startDate === $date;
            });

            if (count($selectedIntervals) > 1) {

                usort($selectedIntervals, function ($a, $b) {
                    if ($a['start'] == $b['start']) {
                        return 0;
                    }
                    return ($a['start'] < $b['start']) ? -1 : 1;
                });

                for ($l = 1; $l < count($selectedIntervals); $l++) {
                    array_push($excluded, [
                        'begin' => $selectedIntervals[$l - 1]['end'],
                        'end' => $selectedIntervals[$l]['start']
                    ]);
                }
            }


            foreach ($selectedIntervals as $interval) {
                $startHour = date('H', strtotime($interval['start']));
                $endHour = date('H', strtotime($interval['end']) - $duration);

                for ($k = (int)$startHour; $k <= (int)$endHour; $k++) {
                    if (!in_array($k, $hours)) {
                        array_push($hours, $k);
                    }
                }
            }

            array_push($result, ['date' => $date, 'hours' => $hours, 'excluded' => $excluded]);

        }

        return $result;
    }

    /**
     * Запрос к службе SOS
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function getAvailableHours($data)
    {

        try {
            $request = $this->postRequest($this->getSite() . '/orders/', null, $data);
            $order = $request -> getData();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        $result['availableHours'] = $this->prepareAvailableHours($order);
        $result['orderData'] = [
            'id' => $order['id'],
            'requests' => $order['requests']
        ];

        return $result;
    }

    public function isHourAvailable($value, $address, $house, $tcId)
    {
        $validating_time = new DateTime($value);

        $requests = $this->getSmartWORequests($house, $address, null, $tcId);
        $params = $this->prepareSmartWoParams($requests);

        $order = $this->getAvailableHours($params['requestData']);

        foreach ($order['availableHours'] as $object) {
            foreach ($object['hours'] as $hour) {
                if ((int)$validating_time->format('H') == $hour) {
                    return true;
                }
            }
        }
        return false;
    }

    public function saveDesiredTime($orderId, $desiredTime)
    {
        try {
            $data = [];
            $data['desiredTime'] = $desiredTime;
            $request = $this->patchRequest($this->getSite() . '/orders/' . $orderId . '/desiredtime', null, $data);
            $order = $request -> getData();
        } catch (\Exception $e) {
            var_dump($e);
            exit;
            return false;
        }

        try {
            $this->putRequest($this->getSite() . '/orders/' . $orderId, null, $order);
        } catch (\Exception $e) {
            var_dump($e);
            exit;
            return false;
        }

        return true;

    }

     /**
     * Подготавливаем заявки для запроса к SOS
     */
    public function getSmartWORequests($house, $address, $technology, $tariffs)
    {
        $requests = [];
        $houseId = $house['id'];

        if (is_array($address)) {
            $flat = $address['flat'];
        } else {
            $addressArray = explode(',', $address);
            $flatIndex = count($addressArray) - 1;
            $flat = $addressArray[$flatIndex];
        }

        if (!$tariffs) {
            $request = SmartWORequest::model()->prepareRequest($houseId, $flat, $technology);
            array_push($requests, $request);
            return $requests;
        }

        $tariffArray = explode(',', $tariffs);

        if (is_array($tariffArray)) {
            foreach ($tariffArray as $tariff) {
                $request = SmartWORequest::model()->prepareRequest($houseId, $flat, $technology, $tariff);
                array_push($requests, $request);
            }

            return $requests;
        }

        return $requests;

    }

     /**
     * Формирование тела запроса к SOS
     */
    public function prepareSmartWoParams($requests)
    {
        $params = array();

        $params['requestData'] = [
            'orderType' => 'VISIT',
            'requests' => $requests
        ];

        return $params;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'date' => $this->day,
            'hours' => $this->hours,
            'mount_area' => $this->mountArea,
        ];
    }
}