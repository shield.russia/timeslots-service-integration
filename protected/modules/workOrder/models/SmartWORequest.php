<?php
namespace crm\modules\workOrder\models;

use ActiveResource;
use CMap;
use EActiveResourceRequest;
use EActiveResourceRequestException;
use HttpException;
use Yii;

/**
 * Используется для работы с rmsi-type-of-service
 * http://rmsi-proxy.rmsi.ntk.novotelecom.ru/rmsi-type-of-service/swagger-ui.html#/
 * Class SmartWORequest
 * Формирует заявку и взаимодействует со службой rmsi-type-of-service для получения сервисов
 * @package crm\modules\jump\models
 */
class SmartWORequest extends ActiveResource
{
    public $id;

    public $service;

    public $connectionTechnology = 'Ethernet';

    public $speed = null;

    public $name = 'TECHSUP';

    public $type = '281';

    public $cityId = '178';

    public $provider = 'ntk';


    const CACHE_KEY_PREFIX = 'SmartWO.Services.';

    /**
     * @param string $className
     * @return SmartWORequest
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function connectionName()
    {
        return 'TypeOfService';
    }

    public function rest()
    {
        return CMap::mergeArray(parent::rest(), [
            'idProperty' => 'id',
            'resource' => 'request'
        ]);
    }

    public function sendRequest($uri, $method, $params, $data)
    {
        if (!empty($params))
            $uri = $uri . '?' . http_build_query($params);

        $request = new EActiveResourceRequest;
        $request->setUri($uri);
        $request->setHeaderField('Authorization', 'Bearer ' . Yii::app()->sosKeyCloak->getAccessToken());
        $request->setMethod($method);
        $request->setData($data);
        return $this->getConnection()->execute($request);
    }

    /**
     * Получаем группы сервисов, по их id система слотирования опеределяет трудозатратность работ
     * @return array
     * @throws EActiveResourceRequestException
     */
    public function getServicesGroups()
    {
        try {
            $result = $this->getRequest($this->getSite() . '/service/all');
        } catch (EActiveResourceRequestException $e) {
            throw new EActiveResourceRequestException($e->getMessage(), $e->getCode());
        }

        return $result->getData();
    }


    /**
     * Устанавливает параметры заявки
     * @param $action
     * @param $connectionTechnology
     * @return bool
     */
    protected function setServiceParams($action, $connectionTechnology)
    {
        if (!$action){
            return false;
        }

        $requestTypes = Yii::app()->params['jira']['tickets_info']['TECHSUP']['requestTypes'];

        if (array_key_exists($action, $requestTypes)){
            $this->name = 'TECHSUP';
            $this->type = $action;

            return false;
        }

        $tariff =  Yii::app()->coreTariffRestService->getById($action);

        if ($tariff){
            if ($tariff->serviceId === 20){
                $props = $tariff->properties;
                $speed = $props['asrProperties']['DOWNLOAD_BW_USER'];
                $this->speed = $speed / 1024;
                $this->connectionTechnology = $connectionTechnology;
            }

            if (in_array($tariff->serviceId, [0,20,515])){
                $this->type = '307';
                $this->name = 'ABNCON';
            } else {
                $this->type = '342';
                $this->name = 'TECHSUP';
            }

        }

    }

    /**
     * Основная функция для получения заявок
     * @param $houseId
     * @param $flat
     * @param $connectionTechnology
     * @param null $action
     * @return array
     * @throws HttpException
     */
    public function prepareRequest($houseId, $flat, $connectionTechnology, $action = null)
    {

        $this->setServiceParams($action, $connectionTechnology);

        $service = $this->createService($this->name, $this->type, $this->speed);

        $serviceId = $service['serviceTypeId'];

        return $this->makeRequest($serviceId, $houseId, $flat);
    }

    /**
     * Формирует заявку для SOS
     * @param $serviceId
     * @param $houseId
     * @param $flat
     * @return array
     */
    protected function makeRequest($serviceId, $houseId, $flat)
    {
        return [
            'cityId' => $this->cityId,
            'flatNumber' => $flat,
            'houseId' => $houseId,
            'providerName' => $this->provider,
            'serviceId' => $serviceId,
            'technology' => $this->connectionTechnology
        ];
    }

    /**
     * Если требуемый сервис не найден - добавляем его сами
     * @param $requestType
     * @param $speed
     * @return array
     * @throws \HttpException
     */
    public function createService($name, $type, $speed)
    {

        $data = [
            'requestName' => $name,
            'requestType' => $type,
            'speed' => $speed
        ];


        try {
            $request = $this->postRequest($this->getSite() . '/requests/', null, $data);
            $result = $request -> getData();
        } catch (HttpException $e) {
            throw new HttpException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

}
