<?php

    use crm\modules\workOrder\models\SmartWorkOrder;

    /**
 * Class AjaxController
 */
class AjaxController extends CController
{
    /**
     * @inheritdoc
     */
    public function filters()
    {
        return [
            'ajaxOnly',
        ];
    }

    /**Подготовка данных из запроса для получения ЖВВ
     * @param $request
     * @return array
     * @throws Exception
     */
    private function prepareParams($request)
    {
        $params = array();

        $address = $request->getParam('address');
        if (is_array($address)) {
            $address = HouseAddress::model()->toString(
                $address['city'],
                $address['street'],
                $address['house'],
                '',
                $address['flat']
            );
        }

        $params['address'] = $address;

        $from = new DateTime($request->getParam('date_from'));
        $params['from'] = $from;
        if ($request->getParam('date_to')) {
            $to = new DateTime($request->getParam('date_to'));
        } else {
            $to = clone $from;
            $to->modify('+1 month');
        }
        $params['to'] = $to;
        $is_vl = $request->getParam('is_vl');
        $params['is_vl'] = $is_vl;
        $area = $request->getParam('area');
        $params['area'] = $area;
        $tcId = $request->getParam('tcId', null);
        $params['tcId'] = $tcId;

        return $params;
    }

    /**Получение ЖВВ
     * @param $params
     * @return array
     */

    private function getHouseData($params)
    {

        $response = ['status' => 'ok', 'address' => $params['address']];
        try {
            $availableHours = AvailableHours::model()->get(
                $params['from']->format(DateHelper::DATE_FORMAT),
                $params['to']->format(DateHelper::DATE_FORMAT),
                $params['address'],
                $params['is_vl'],
                $params['area'],
                $params['tcId']
            );
            foreach ($availableHours as $index => $object) {
                $availableHours[$index] = $object->toArray();
            }
            $response['availableHours'] = $availableHours;
            $response['holidays'] = Yii::app()->broadway->getHolidays((new DateTime('first day of -1 month 00:00:00'))->format('Y-m-d'));
        } catch (EActiveResourceRequestException $e) {
            $response = [
                'status' => 'error',
                'error' => $e->getMessage(),
            ];
        } catch (Exception $e) {
            $response = [
                'status' => 'error',
                'error' => $e->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * Отдача расписания в JSON
     * В зависимости от территории выбирается какую систему слотирования дергать
     *
     * @throws Exception
     */

    public function actionGetAvailableHours()
    {
        $request = Yii::app()->request;
        $address = $request->getParam('address');
        $house = House::model()->getHouseByAddress($address);
        $smartWOAreas = Yii::app()->params['jira']['SOS_areas'];

        if (in_array($house['mount_area'], $smartWOAreas)) {
            $technology = $request->getParam('technology')
                ? $request->getParam('technology')
                : 'ETHERNET';
            $requests = SmartWorkOrder::model()->getSmartWORequests($house, $address, $technology, $request->getParam('tcId'));
            $params = SmartWorkOrder::model()->prepareSmartWoParams($requests);
            if (is_array($address)) {
                $params['address'] = HouseAddress::model()->toString(
                    $address['city'],
                    $address['street'],
                    $address['house'],
                    '',
                    $address['flat']
                );
            } else {
                $params['address'] = $address;
            }

            $response = $this->getTimeSlots($params);
        } else {
            $params = $this->prepareParams($request);
            $response = $this->getHouseData($params);
        }

        echo CJSON::encode($response);

    }

    /**
     * Получение свободных часов из SOS
     */

    public function getTimeSlots($params)
    {
        $response = ['status' => 'ok', 'address' => $params['address']];
        try {
            $smartOrder = SmartWorkOrder::model()->getAvailableHours($params['requestData']);
            $response['availableHours'] = $smartOrder['availableHours'];
            $response['smartOrder'] = $smartOrder['orderData'];
            $response['holidays'] = Yii::app()->broadway->getHolidays((new DateTime('first day of -1 month 00:00:00'))->format('Y-m-d'));
        } catch (EActiveResourceRequestException $e) {
            $response = [
                'status' => 'error',
                'error' => $e->getMessage(),
            ];
        } catch (Exception $e) {
            $response = [
                'status' => 'error',
                'error' => $e->getMessage(),
            ];
        }
        return $response;

    }

    /*
     * Валидация ЖВВ
     */

    public function actionValidateDesiredTime()
    {
        $request = Yii::app()->request;
        $smartOrderId = $request->getParam('sosId');
        $params = $this->prepareParams($request);
        $desiredTime = new DateTime($request->getParam('desiredTime'));
        $desiredDay = $desiredTime->format('Y-m-d');
        $desiredHour = (int)$desiredTime->format('H');

        if ($smartOrderId){
            $result = $this->validateSmartOrderTime($smartOrderId, $desiredTime);
        } else {
            $result = $this->validateWorkorderTime($params, $desiredHour, $desiredDay);
        }

        echo CJSON::encode($result);
    }

    protected function validateWorkorderTime($params, $desiredHour, $desiredDay){
        $houseData = $this->getHouseData($params);

        foreach ($houseData['availableHours'] as $day) {
            if ($day['date'] == $desiredDay) {
                $hours_array = $day['hours'];
            }
        }

        return in_array($desiredHour, $hours_array);

    }

    protected function validateSmartOrderTime($orderId, $desiredTime){
        return SmartWorkOrder::model()->saveDesiredTime($orderId, $desiredTime->format('Y-m-d\TH:i:s.vP'));
    }

}
